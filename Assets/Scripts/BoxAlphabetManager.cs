﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class BoxAlphabetManager : MonoBehaviour
{
    public Transform positionOrigin;
    public GameObject prefabBoxAlphabet;
    public float spacingX = 1;
    public float spacingY = 1;

    internal void GenerateMap(Data data)
    {
        Vector3 pos = positionOrigin.position;
        for (int i = 0; i < data.words.Count; i++)
        {
        

            var alphabet = data.words[i].ToCharArray();
            for (int k = 0; k < alphabet.Length; k++)
            {
                var go = Instantiate(prefabBoxAlphabet, new Vector3(pos.x + spacingX * k, pos.y - spacingY * i, pos.z), Quaternion.identity, transform);
                go.GetComponent<BoxAlphabet>().SetValue(alphabet[k], data.dictWordsData[data.words[i]]);
            }
        }
    }
}
