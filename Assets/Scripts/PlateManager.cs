﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PlateManager : MonoBehaviour
{
    public GameObject prefabMap;
    public float radius = 1;

    public void SpawnAlphabet(string[] alphabets)
    {
        for (int i = 0; i < alphabets.Length; i++)
        {

            float angle = i * Mathf.PI * 2f / alphabets.Length;
            Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius, 0) + transform.position;

            var go = Instantiate(prefabMap, newPos, Quaternion.identity, transform);
            go.GetComponent<AlphabetObject>().SetValue(alphabets[i]);
        }
    }
}
