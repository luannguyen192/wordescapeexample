﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UniRx;

public class BoxAlphabet : MonoBehaviour
{
    public bool activeWhenSpawn = true;
    public TextMeshPro textMeshPro;

    public void SetValue(char alphabet, UniRx.BoolReactiveProperty show)
    {
        textMeshPro.text = alphabet.ToString();
        textMeshPro.gameObject.SetActive(activeWhenSpawn);
        show.TakeUntilDestroy(gameObject).Subscribe(value =>
        {
            textMeshPro.gameObject.SetActive(value);
        });
    }
}
