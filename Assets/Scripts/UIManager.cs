﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text Level;
    public GameObject panel;

    private void Start()
    {
        GeneratorMapManager.Singleton.currentLevel.Subscribe(lvl =>
        {
            Level.text = "Level " + lvl;
        });
        GeneratorMapManager.Singleton.complete.Subscribe(value =>
        {
            panel.gameObject.SetActive(value);
        });
    }

    public void OnClickPlay()
    {
        GeneratorMapManager.Singleton.CreateLevel();
    }

}
