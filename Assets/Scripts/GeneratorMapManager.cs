﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using TMPro;

[System.Serializable]
public class Data
{
    public string[] alphabets;
    public List<string> words;
    [System.NonSerialized]
    public Dictionary<string, BoolReactiveProperty> dictWordsData;
    public void Init()
    {
        dictWordsData = new Dictionary<string, BoolReactiveProperty>();
        foreach (var item in words)
        {
            dictWordsData.Add(item, new BoolReactiveProperty(false));
        }
    }
}

public class GeneratorMapManager : MonoBehaviour
{
    static GeneratorMapManager instance;
    public static GeneratorMapManager Singleton
    {
        get
        {
            if (instance == null)
            {
                Debug.Log("You Should create instance GeneratorMapManager first");
            }
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
        foreach (var item in datas)
        {
            item.Init();
        }
    }

    public IntReactiveProperty currentLevel;

    public List<Data> datas = new List<Data>();

    public PlateManager plateManager;

    public BoxAlphabetManager boxAlphabetManager;

    public LineRenderer lineRenderer;

    public int indexLinerender;

    public TextMeshPro text;

    public List<int> gameobjectID = new List<int>();

    public int numberAnswer;

    public BoolReactiveProperty complete;

    public void CreateLevel()
    {
        foreach (var item in datas)
        {
            foreach (var word in item.dictWordsData)
            {
                word.Value.Value = false;
            }
        }

        for (int i = plateManager.transform.childCount; i > 0; i--)
        {
            Destroy(plateManager.transform.GetChild(i-1).gameObject);
        }
        for (int i = boxAlphabetManager.transform.childCount; i > 0; i--)
        {
            Destroy(boxAlphabetManager.transform.GetChild(i-1).gameObject);
        }

        numberAnswer = datas[currentLevel.Value].words.Count;
        complete.Value = false;
        GenerateWordPlate(datas[currentLevel.Value]);
        GenerateWordMap(datas[currentLevel.Value]);
    }

    async public void GenerateWordMap(Data data)
    {
        boxAlphabetManager.GenerateMap(data);
    }

    async public void GenerateWordPlate(Data data)
    {
        plateManager.SpawnAlphabet(data.alphabets);
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            gameobjectID.Clear();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            indexLinerender = 0;
            if (Physics.Raycast(ray, out hitInfo))
            {
                gameobjectID.Add(hitInfo.transform.GetInstanceID());
                lineRenderer.SetPosition(indexLinerender, hitInfo.transform.position);
                lineRenderer.SetPosition(indexLinerender + 1, hitInfo.transform.position);
                indexLinerender++;
                text.text = hitInfo.transform.GetComponent<AlphabetObject>().alphabet;
            }
        }

        if (Input.GetMouseButton(0))
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            lineRenderer.SetPosition(indexLinerender, new Vector3(pos.x, pos.y, 0));

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo) && !gameobjectID.Contains(hitInfo.transform.GetInstanceID()))
            {
                gameobjectID.Add(hitInfo.transform.GetInstanceID());
                text.text = text.text + hitInfo.transform.GetComponent<AlphabetObject>().alphabet;
                indexLinerender++;
                lineRenderer.positionCount = indexLinerender + 1;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (datas[currentLevel.Value].words.Contains(text.text))
            {
                datas[currentLevel.Value].dictWordsData[text.text].Value = true;
                numberAnswer--;

                if (numberAnswer == 0)
                {
                    text.text = "COMPLETE";
                    complete.Value = true;
                    currentLevel.Value++;
                    if (currentLevel.Value >= datas.Count)
                    {
                        currentLevel.Value = 0;
                    }
                }
            }
            indexLinerender = 0;
            lineRenderer.positionCount = indexLinerender + 2;
            lineRenderer.SetPosition(indexLinerender, Vector3.zero);
            lineRenderer.SetPosition(indexLinerender + 1, Vector3.zero);
        }

    }
}
