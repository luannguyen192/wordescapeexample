﻿using TMPro;
using UnityEngine;

public class AlphabetObject : MonoBehaviour
{
    public TextMeshPro textmeshPro;
    public string alphabet;
    public void SetValue(string alphabet)
    {
        this.alphabet = alphabet;
        textmeshPro.text = alphabet;
    }

}
